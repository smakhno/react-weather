import React, { Component } from 'react';

class ForecastItem extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: ''
		};

	}

	render() {
	    return (
		    <div className="forecast">
				<div className="forecast-header">
					<div className="day">1111</div>
				</div>
				<div class="forecast-content">
					<div className="forecast-icon">
						<img src="" alt="" width="48"/> 
					</div>
					<div className="degree">11<sup>o</sup>C</div>
					<small>12<sup>o</sup></small>
				</div>
			</div>
	    );
	}
}

export default ForecastItem;
