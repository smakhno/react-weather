import React, { Component } from 'react';

class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: ''
		};

	}

	submitSearch(){

	}

	handleSearchChange(event) {
		this.setState({search: event.target.value});
	}

	render() {
	    return (
		    <div className="hero" data-bg-image="images/banner.png">
				<div className="container">
					<form action="#" className="find-location">
						<input type="text" placeholder="Find your location..." value={this.state.search} onChange={this.handleSearchChange.bind(this)}/>
						<input type="button" className="submit" value="Find" onClick={this.submitSearch.bind(this)}/>
					</form>

				</div>
			</div>
	    );
	}
}

export default Search;
