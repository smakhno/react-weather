import React, { Component } from 'react';
import ForecastMain from './ForecastMain';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      data: ''
    };

  }

  getUrl() {
    var api_key = '6e3fcb812a6ea26bfbb60bacee7afa6f';
    var url = 'http://api.openweathermap.org/data/2.5/forecast?q=' + this.state.search + '&units=metric&appid=' + api_key;
    return url;
  }

  getForecast(url) {
    return new Promise(function(resolve, reject) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.onload = function() {
        if (this.status === 200) {
          resolve(this.response);
        } 
      };
      xhr.send();
    });
  }

    submitSearch() {
    var url = this.getUrl();
    console.log(url);
    this.getForecast(url).then(data => this.setState({data: data}));
  }

  handleSearchChange(event) {
    this.setState({search: event.target.value});
  }

  render() {
    return (
      <div>
        <div className="container searchFieldCont">
          <form action="#" className="find-location">
            <input type="text" placeholder="Find your location..." value={this.state.search} onChange={this.handleSearchChange.bind(this)}/>
            <input type="button" className="submit" value="Find" onClick={this.submitSearch.bind(this)}/>
          </form>
        </div>
        <ForecastMain data={this.state.data}/>
        <div>{this.state.data}</div>
      </div>
    );
  }
}

export default App;
