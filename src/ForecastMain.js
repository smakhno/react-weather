import React, { Component } from 'react';
import ForecastItem from './ForecastItem'

class ForecastMain extends Component {

	render() {
		if (!this.props.data) return <div></div>;
	    return (
	    	<div className="forecast-table">
			    <div className="container">
					<div className="forecast-container">
						<div className="today forecast">
							<div className="forecast-header">
								<div className="day">11</div>
								<div className="date">11</div>
							</div> 
							<div className="forecast-content">
								<div className="location">{{this.props.data.name}}</div>
								<div className="degree">
									<div className="num">12<sup>o</sup>C</div>
									<div className="forecast-icon">
										<img src="" alt="" width="90"/>
									</div>	
									<span></span>
								</div>
									<span><img src="images/icon-umberella.png" alt=""/>%</span>
									<span><img src="images/icon-wind.png" alt=""/>m/s</span>
									<span><img src="images/icon-compass.png" alt="" /></span>
								</div>
						</div>
						<ForecastItem/>
					</div>
				</div>
			</div>
	    );
	}
}

export default ForecastMain;
